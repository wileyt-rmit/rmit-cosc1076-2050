
#include <iostream>
#include <string>
#include <vector>

int main(void) {
   std::vector<int> vec;

   for (int i = 0; i != 10; ++i) {
      vec.push_back(i);
   }

   // for (int& value : vec) {
   //    std::cout << value << std::endl;
   // }

   for (std::vector<int>::const_iterator it = vec.begin();
        it != vec.end();
        ++it) {
      int value = *it;
      std::cout << value << std::endl;
   }

   // for (int& value : vec) {
   //    std::cout << value << std::endl;
   // }

   return EXIT_SUCCESS;
}
