#include <iostream>

#define LENGTH 10

template<typename T>
T multiply(T x, T y) {
   return x * y;
}

class Tmp {
public:
   int x;
};

int main(void) {

   double x = 7.6;
   double y = 8.0;

   std::cout << multiply<double>(y,x) << std::endl;

   bool b1 = true;
   bool b2 = false;
   std::cout << multiply<bool>(b1,b2) << std::endl;

   char c1 = 'a';
   char c2 = 'z';
   std::cout << multiply<char>(c1,c2) << std::endl;

   // double arr1[LENGTH] = {1};
   // double arr2[LENGTH] = {1};
   // std::cout << multiply<double*>(arr1, arr2) << std::endl;

   Tmp t1;
   Tmp t2;
   std::cout << multiply<Tmp>(t1,t2) << std::endl;

   return EXIT_SUCCESS;
}
