#include <iostream>

#include "Card.h"

int main(void) {

   Card* card1 = new Card(RED, 7);
   Card* card2 = new Card(RED, 6);
   Card* card3 = new Card(ORANGE, 7);
   Card* card4 = new Card(RED, 7);

   bool equal = *card1 == *card2;
   std::cout << equal << std::endl;

   equal = *card1 == *card3;
   std::cout << equal << std::endl;

   equal = *card1 == *card4;
   std::cout << equal << std::endl;





   Card a(RED,7);
   Card b(RED,6);
   // Card c(b);

   a = b;

   return EXIT_SUCCESS;
}
 