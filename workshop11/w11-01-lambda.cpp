
#include <iostream>
#include <string>

// void foo();

int main(void) {

   int x = 1;
   int y = 2;

   // Lambda function
   // - capture variables from the outside scope
   auto compare = [=](int& i1, int& i2) {
      if (i1 == x) {
         std::cout << "i1 == x" << std::endl;
      } else {
         std::cout << "i1 != x" << std::endl;
      }
      // x = 2;
   };

   compare(x, y);

   compare(x, x);
   compare(y,y);

   // foo();

   return EXIT_SUCCESS;
}

// void foo() {
//    std::cout << "foo()" << std::endl;
//    int x = 7;
//    int y = 8;
//    // compare(x,y);
// }
