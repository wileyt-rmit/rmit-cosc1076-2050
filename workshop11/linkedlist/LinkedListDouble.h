#ifndef LINKED_LIST_DOUBLE_H
#define LINKED_LIST_DOUBLE_H 

#include "LinkedList.h"

class Node {
public:
   Node(int value, Node* next, Node* prev);

   int value;
   Node* next;
   Node* prev;
};

class LinkedListDouble : public LinkedList {
public:

   LinkedListDouble();
   LinkedListDouble(const LinkedListDouble& other);
   virtual ~LinkedListDouble();

   /**
    * Return the current size of the Linked List.
    */
   virtual unsigned int size() const;

   /**
    * output: Get the value at the given index.
    * input: Index must be >=0 and < size()
    * 
    */
   virtual int get(const unsigned int index) const;

   /**
    * Add the value to the back of the Linked List
    */
   virtual void addBack(int value);

   /**
    * Add the value to the front of the Linked List
    */
   virtual void addFront(int value);

   /**
    * Remove the value at the back of the Linked List
    */
   virtual void removeBack();

   /**
    * Remove the value at the front of the Linked List
    */
   virtual void removeFront();

   /**
    * Removes all values from the Linked List
    */
   // virtual void clear();

private:

   Node* head;
   Node* tail;
};

#endif // LINKED_LIST_DOUBLE_H
