#ifndef LINKED_LIST_H
#define LINKED_LIST_H 

// class Node {
// public:
//    Node(int value, Node* next);

//    int value;
//    Node* next;
// };

class LinkedList {
public:

   /**
    * Return the current size of the Linked List.
    */
   virtual unsigned int size() const = 0;

   /**
    * output: Get the value at the given index.
    * input: Index must be >=0 and < size()
    * 
    */
   virtual int get(const unsigned int index) const = 0;

   /**
    * Add the value to the back of the Linked List
    */
   virtual void addBack(int value) = 0;

   /**
    * Add the value to the front of the Linked List
    */
   virtual void addFront(int value) = 0;

   /**
    * Remove the value at the back of the Linked List
    */
   virtual void removeBack() = 0;

   /**
    * Remove the value at the front of the Linked List
    */
   virtual void removeFront() = 0;

   /**
    * Removes all values from the Linked List
    */
   virtual void clear();

};

#endif // LINKED_LIST_H
