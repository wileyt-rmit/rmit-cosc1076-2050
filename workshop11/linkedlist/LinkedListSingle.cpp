
#include "LinkedListSingle.h"

#include <exception>
#include <iostream>
#include <fstream>
#include <limits>

Node::Node(int value, Node* next) :
   value(value),
   next(next)
{}

LinkedListSingle::LinkedListSingle() 
{
   head = nullptr;
}

LinkedListSingle::LinkedListSingle(const LinkedListSingle& other) 
{
   // TODO
}

LinkedListSingle::~LinkedListSingle() {
   clear();
}

unsigned int LinkedListSingle::size() const {
   unsigned int count = 0;
   Node* current = head;
   while(current != nullptr) {
      ++count;
      current = current->next;
   }

   return count;
}

int LinkedListSingle::get(const unsigned int index) const {
   int count = 0;
   Node* current = head;
   int returnValue = 0;
   // int returnValue = std::numeric_limits<int>::min();
   if (index < size()) {
      while(count < index) {
         ++count;
         current = current->next;
      }
      returnValue = current->value;
   } else {
      throw std::out_of_range("Linked List get - index out of range");
   }
   
   return returnValue;
}

void LinkedListSingle::addFront(int value) {
   // TODO
}

void LinkedListSingle::addBack(int value) {
   Node* toAdd = new Node(value, nullptr);

   if (head == nullptr) {
      head = toAdd;
   } else {
      Node* current = head;
      while(current->next != nullptr) {
         current = current->next;
      }

      current->next = toAdd;
   }
}

void LinkedListSingle::removeBack() {
   // TODO
}

void LinkedListSingle::removeFront() {
   Node* current = head->next;
   delete head;
   head = current;
}

