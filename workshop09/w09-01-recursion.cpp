#include <iostream>
#include <string>

int factorial(int n);

int main(void) {

   // Implement factorial
   std::cout << factorial(5) << std::endl;

   return EXIT_SUCCESS;
}

int factorial(int n) {
   int retVal = 0;
   if (n == 0) {
      retVal = 1;
   } else if (n == 1) {
      retVal = 1;
   } else {
      // ASSUME THAT factorial(n-1) is CORRECT
      retVal = factorial(n-1) * n;
   }

   return retVal;
}

// Calculate the n-th fibonacci number
// 0 1 1 2 3 5 8 13 21 34
// 1 2 3 4 5 6 7 8  9  10  <- nth number
int fibonacci(int n) {
   int retVal = 0;
   
   if (n == 0) {
      retVal = 0;
   } else if (n == 1) {
      retVal = 1;
   } else {
      // Recursive step
      retVal = fibonacci(n-1) + fibonacci(n-2);
   }

   return retVal;
}

// kayak
// mum
// racecar
// abcdefggfedcba

bool palindrome(char* string, int start, int last) {
   bool retVal = false;
   
   if (last < start) {
      // empty string
      retVal = true;
   } else if (start == last) {
      // string of 1 element
      retVal = true;
   } else {
      // Recursive step
      retVal = palindrome(string, start + 1, last - 1) &&
               string[start] == string[last];
   }

   return retVal;
}

// Must be a valid c-string (not null) <- must be null-terminted
bool palindrome(char* string) {
   int length = strLen(string);
   return palindrome(string, 0, length - 1);
}

