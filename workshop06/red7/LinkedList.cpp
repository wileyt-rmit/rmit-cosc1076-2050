#include "LinkedList.h"

LinkedList::LinkedList() {
   head = nullptr;
}

LinkedList::LinkedList(LinkedList& other) {
}

LinkedList::~LinkedList() {
}

int LinkedList::size() {
   int counter = 0;

   Node* current = head;
   while (current != nullptr) {
      current = current->next;
      ++counter;
   }

   return counter;
}

Card* LinkedList::get(int index) {

   Card* retCard = nullptr;

   if (index >= 0 && index < size()) {
      int counter = 0;
      Node* current = head;
      while (counter < index) {
         current = current->next;
         ++counter;
      }

      retCard = current->card;
   }

   return retCard;
}

void LinkedList::add_front(Card* data) {
   Node* node = new Node();
   node->card = data;
   node->next = head;
   head = node;
}

void LinkedList::add_back(Card* data) {

}

void LinkedList::remove_front() {

}

void LinkedList::remove_back() {

}