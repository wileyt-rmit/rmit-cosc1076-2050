
#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

void foo(int x);
void foo(double x);

#include "linkedlist/LinkedList.h"
#include "linkedlist/LinkedListSize.h"

int main(void) {

   int a = -8;
   double b = 6.8;

   float f = static_cast<float>(1.9);

   auto x = 1;
   cout << x / 2 << endl;

   foo(7);
   foo(a);
   foo(x);
   foo(b);

   foo(static_cast<double>(1.0f));
   foo((int) f);
   foo((double) 'a');


   std::cout << "****** " << std::endl << std::endl;

   // Typecasting with classes
   // LinkedList* list;
   // LinkedListSize* llSize = new LinkedListSize();

   // // Polymorphism
   // list = llSize;
   // cout << list->size() << endl;

   // // Static
   // list = nullptr;
   // list = static_cast<LinkedList*>(llSize);
   // cout << list->size() << endl;
   
   // list = nullptr;
   // list = dynamic_cast<LinkedList*>(llSize);
   // cout << list->size() << endl;


   // void foo(LinkedList* list)
   // Is the parent **actually** a child?
   // Is this **actually** a llSize??

   // Use casting to tell us the answer

   {
      // LinkedList* list = new LinkedListSize();
      LinkedList* list = new LinkedList();
      cout << "+++:" << list->size() << endl;
      

      // Is this **actually** a llSize??
      // DYNAMIC Cast lets us see the underlying type of an object
      //     AT RUNTIME!!
      LinkedListSize* llSize = dynamic_cast<LinkedListSize*>(list);
      // LinkedListSize* llSize = static_cast<LinkedListSize*>(list);
      if (llSize != nullptr) {
         cout << "***: " << llSize->size() << endl;
      } else {
         cout << "Nullptr" << endl;
      }

      delete list;
   }




   return EXIT_SUCCESS;
}


void foo(int x) {
   cout << "Int foo" << endl;
}

void foo(double x) {
   cout << "Double foo" << endl;
}

