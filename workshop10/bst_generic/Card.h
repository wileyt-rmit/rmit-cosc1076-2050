
#ifndef CARD_H
#define CARD_H

enum Colour {
   RED,
   ORANGE,
   YELLOW,
   GREEN,
   BLUE,
   INDIGO,
   VIOLET
};

class Card {
public:
   // Card();
   Card(Colour colour, int number);
   Card(Card& other);
   Card(Card&& other);
   ~Card();

   Colour getColour();
   int getNumber();

private:
   Colour colour;
   int number;
};

bool lessthan(Card* card1, Card* card2) {
   return card1->getNumber() < card2->getNumber()||
          card1->getColour() < card2->getColour();
}

void printdata(Card* card) {
   std::cout << "Card(" << card->getColour() << ", "
             << card->getNumber() << ")" << std::endl;
}

#endif // CARD_H
