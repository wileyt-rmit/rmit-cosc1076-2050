
#ifndef WEEK09_BST_NODE_H
#define WEEK09_BST_NODE_H

#include <memory>
#include <iostream>

template<typename T>
class BST_Node {
public:

   BST_Node(T value);
   BST_Node(const BST_Node<T>& other);
   ~BST_Node();

   T   value;

   std::shared_ptr<BST_Node> left;
   std::shared_ptr<BST_Node> right;
};

using std::string;
using std::cout;
using std::endl;

template<typename T>
BST_Node<T>::BST_Node(T value) :
   value(value),
   left(nullptr),
   right(nullptr)
{
   cout << "BST_Node Create: " << value << endl;
}

template<typename T>
BST_Node<T>::BST_Node(const BST_Node<T>& other) :
   value(other.value),
   left(other.left),
   right(other.right)
{
   cout << "BST_Node Copy: " << value << endl;
}

template<typename T>
BST_Node<T>::~BST_Node() {
   cout << "BST_Node Deconstructor: " << value << endl;
   left = nullptr;
   right = nullptr;
}

#endif // WEEK09_BST_NODE_H
