
#include "BST.h"

#include <iostream>
#include <string>

#include "Card.h"

using std::string;
using std::cout;
using std::endl;

int main(void) {
   // BST<double*>* bst = new BST<double>();

   // bst->add(new double (7.1));
   // bst->add(3.2);
   // bst->add(1);
   // bst->add(2);
   // bst->add(10);
   // bst->add(8);
   // bst->add(11);

   // // std::cout << "CLEAR!!" << std::endl;
   // // bst->clear();
   // // std::cout << "done clear" << std::endl;

   // for (int i = 0; i != 15; ++i) {
   //    cout << "Bst contains " << i << "? " << bst->contains(i) << endl;
   // }

   // std::cout << "***** DFS" << std::endl;
   // bst->dfs();

   // delete bst;
   // cout << "Done" << endl;

   // std::vector<Card*>

   BST<Card*>* bst = new BST<Card*>();

   bst->add(new Card(RED, 7));
   bst->add(new Card(RED, 1));
   bst->add(new Card(GREEN, 1));
   bst->add(new Card(GREEN, 7));

   std::cout << "***** DFS" << std::endl;
   bst->dfs();

   return EXIT_SUCCESS;
}
