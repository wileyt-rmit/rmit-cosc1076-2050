
#ifndef WEEK09_BST_H
#define WEEK09_BST_H

#include "BST_Node.h"

#include <memory>
#include <iostream>

#include "Card.h"

template<typename T>
class BST {
public:

   BST();
   ~BST();

   void clear();
   
   // contract: we don't modify the data given
   //           we don't modify the BST
   bool contains(const T data) const;

   // contract: we don't modify the data given
   void add(const T data);

   // Do a Depth First Search of the BST
   void dfs() const;

private:
   std::shared_ptr<BST_Node<T> > root;

   bool contains(std::shared_ptr<BST_Node<T> > node, 
                 const T data) const;

   std::shared_ptr<BST_Node<T> > add(std::shared_ptr<BST_Node<T> > node, 
                                 const T data);

   void dfs(std::shared_ptr<BST_Node<T> > node) const;
};

template<typename T>
BST<T>::BST() {
   root = nullptr;
}

template<typename T>
BST<T>::~BST() {
}

template<typename T>
void BST<T>::clear() {
   root = nullptr;
}

template<typename T>
bool BST<T>::contains(const T data) const {
   return contains(root, data);
}

template<typename T>
bool BST<T>::contains(std::shared_ptr<BST_Node<T> > node, 
                   const T data) const {
   bool returnValue = false;

   if (node == nullptr) {
      returnValue = false;
   } else if (data == node->value) {
      returnValue = true;
   } else {
      // recursive case
      // check which side of the tree to search in
      // if (data < node->value) {
      if (lessthan(data, node->value)) {
         // go left
         returnValue = contains(node->left, data);
      } else {
         // go right
         returnValue = contains(node->right, data);
      }
   }

   return returnValue;
}

template<typename T>
void BST<T>::add(const T data) {
   std::cout << "*** Insert " << data << std::endl;
   root = add(root, data);
}

template<typename T>
std::shared_ptr<BST_Node<T> > BST<T>::add(std::shared_ptr<BST_Node<T> > node, 
                                   const T data) {

   // Please start here VSCODE
   std::shared_ptr<BST_Node<T> > returnNode;

   if (node == nullptr) {
      // base case
      std::cout << "Make new new node: " << data << std::endl;
      returnNode = std::make_shared<BST_Node<T> >(data);
   } else {
      // recursive case
      // check which side of the tree to add the data to
      if (lessthan(data, node->value)) {
         // go left
         std::cout << "\tInsert: " << data << " left of: " << node->value << std::endl;
         std::shared_ptr<BST_Node<T> > subtree = add(node->left, data);
         node->left = subtree;
         returnNode = node;
      } else {
         // go right
         std::cout << "\tInsert: " << data << " right of: " << node->value << std::endl;
         std::shared_ptr<BST_Node<T> > subtree = add(node->right, data);
         node->right = subtree;
         returnNode = node;
      }
   }

   return returnNode;
}

template<typename T>
void BST<T>::dfs() const {
   std::cout << "DFS:" << std::endl;
   dfs(root);
}

template<typename T>
void BST<T>::dfs(std::shared_ptr<BST_Node<T> > node) const {
   // print out all node in the BST
   // go left
   if (node != nullptr) {
      dfs(node->left);

      // do me
      // std::cout << " ";
      printdata(node->value);
      //  << std::endl;

      // go right
      dfs(node->right);
   }
}

#endif // WEEK09_BST_H
