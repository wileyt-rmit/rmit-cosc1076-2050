#include <iostream>

#include "LinkedList.h"

int main(void) {

   LinkedList<char>* list = new LinkedList<char>();
   try {
      list->addBack('a');
      list->addBack('z');
      list->addBack('y');
      list->addBack('$');
   } catch (...) {
      // do something
   }

   try {
      for (unsigned int i = 0; i != list->size(); ++i) {
         std::cout << "list[" << i << "] = " << list->get(i) << std::endl;
      }
   } catch (std::exception& e) {
      std::cout << "General exception: " << e.what() << std::endl;
   }


   return EXIT_SUCCESS;
}
