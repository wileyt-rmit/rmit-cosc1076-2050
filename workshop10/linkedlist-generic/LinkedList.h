#ifndef LINKED_LIST_H
#define LINKED_LIST_H 

template<typename T>
class Node {
public:
   Node(T value, Node* next);

   T value;
   Node* next;
};

template<typename T>
class LinkedList {
public:
   LinkedList();
   LinkedList(const LinkedList& other);
   ~LinkedList();

   /**
    * Return the current size of the Linked List.
    */
   unsigned int size() const;

   /**
    * output: Get the value at the given index.
    * input: Index must be >=0 and < size()
    * 
    */
   T get(const unsigned int index) const;

   /**
    * Add the value to the back of the Linked List
    */
   void addBack(T value);

   /**
    * Add the value to the front of the Linked List
    */
   void addFront(T value);

   /**
    * Remove the value at the back of the Linked List
    */
   void removeBack();

   /**
    * Remove the value at the front of the Linked List
    */
   void removeFront();

   /**
    * Removes all values from the Linked List
    */
   void clear();

private:

   Node<T>* head;
};

#include <exception>
#include <iostream>
#include <fstream>
#include <limits>

template<typename T>
Node<T>::Node(T value, Node* next) :
   value(value),
   next(next)
{}

template<typename T>
LinkedList<T>::LinkedList() 
{
   head = nullptr;
}

template<typename T>
LinkedList<T>::LinkedList(const LinkedList& other) 
{
   // TODO
}

template<typename T>
LinkedList<T>::~LinkedList() {
   clear();
}

template<typename T>
unsigned int LinkedList<T>::size() const {
   unsigned int count = 0;
   Node<T>* current = head;
   while(current != nullptr) {
      ++count;
      current = current->next;
   }

   return count;
}

template<typename T>
T LinkedList<T>::get(const unsigned int index) const {
   int count = 0;
   Node<T>* current = head;
   T returnValue = 0;
   // int returnValue = std::numeric_limits<int>::min();
   if (index < size()) {
      while(count < index) {
         ++count;
         current = current->next;
      }
      returnValue = current->value;
   } else {
      throw std::out_of_range("Linked List get - index out of range");
   }
   
   return returnValue;
}

template<typename T>
void LinkedList<T>::addFront(T value) {
   // TODO
}

template<typename T>
void LinkedList<T>::addBack(T value) {
   Node<T>* toAdd = new Node<T>(value, nullptr);

   if (head == nullptr) {
      head = toAdd;
   } else {
      Node<T>* current = head;
      while(current->next != nullptr) {
         current = current->next;
      }

      current->next = toAdd;
   }
}

template<typename T>
void LinkedList<T>::removeBack() {
   // TODO
}

template<typename T>
void LinkedList<T>::removeFront() {
   Node<T>* current = head->next;
   delete head;
   head = current;
}

template<typename T>
void LinkedList<T>::clear() {
   while (head != nullptr) {
      removeFront();
   }
}

#endif // LINKED_LIST_H
