
#include <iostream>
#include <string>


using std::string;
using std::cout;
using std::endl;


void foo(int x);
void foo(double x);

int main(void) {
   auto a = -8;
   auto b = 6.8;
   auto c = 10.0;
   auto d = &a;
   auto e = &c;
   auto f = "hello world";
   auto g = std::string("hello world");


   cout << a << endl;
   cout << b << endl;
   cout << c << endl;
   cout << d << endl;
   cout << e << endl;
   cout << f << endl;
   cout << g << endl;
   // cout << g.size() << endl;

   // cout << f + "!" << endl;
   cout << g + "!" << endl;

   auto x = 1;
   cout << x / 2 << endl;

   return EXIT_SUCCESS;
}

void foo(int x) {
   cout << "Int foo" << endl;
}

void foo(double x) {
   cout << "Double foo" << endl;
}
