// int pow(int base, int exponent);

/**
 * pre-condition: T MUST be a numeric data type
 */ 
template<typename T>
T pow(T base, int exponent) {
   T result = 1;
   for (int i = 0; i != exponent; ++i) {
      result *= base;
   }
   return result;
}