#include <iostream>

class A {
public:
   A(int x) : x(x) {};
   virtual ~A() {
      std::cout << "~A" << std::endl;
   };

   virtual int getX() { 
      std::cout << "A::getX" << std::endl;
      return x;
   };

protected:
   int x;
};

class B : public A {
public:
   B() :
      A(7) {
         y = new int(-1);
      };
   B(int y) :
      A(5) {
         this->y = new int(y);
      };
   virtual ~B() {
      std::cout << "~B" << std::endl;
      delete y;
   };

   virtual int getX() { 
      std::cout << "B::getX" << std::endl;
      return x * 2;
   };
   int getY() { return *y; };

   void setX(int newX) { x = newX; };

private:
   int* y;
};

class C : public B {
public:
   C() :
      B(7) {
         z = new int(-1);
      };
   virtual ~C() {
      std::cout << "~C" << std::endl;
      delete z;
   };

   int getZ() { return *z; };

   virtual int getX() { 
      std::cout << "C::getX" << std::endl;
      return A::getX();
   };

private:
   int* z;
};

int main(void) {

   A a(10);
   std::cout << a.getX() << std::endl;

   B b(6);
   b.setX(200);
   std::cout << b.getX() << std::endl;
   std::cout << b.getY() << std::endl;

   C c;
   std::cout << c.getX() << std::endl;
   std::cout << c.getY() << std::endl;
   std::cout << c.getZ() << std::endl;

   // delete c;
   std::cout << "***" << std::endl;

   std::cout << "Polymorphic C" << std::endl;
   B* polymorpic = &c;
   std::cout << polymorpic->getX() << std::endl;
   std::cout << polymorpic->getY() << std::endl;
   // std::cout << polymorpic->getZ() << std::endl;
   std::cout << "***" << std::endl;

   return EXIT_SUCCESS;
}
