#include <iostream>

#include "utils.h"

#include "../lecture09/red7/Card.h"

// int pow(int base, int exponent);
// float pow(float base, int exponent);
// double pow(double base, int exponent);

// template<typename T>
// T pow(T base, int exponent) {
//    T result = 1;
//    for (int i = 0; i != exponent; ++i) {
//       result *= base;
//    }
//    return result;
// }

int main(void) {

   int x = 5;
   // double y = 1.5;
   int exponent = 3;

   int resultI = pow<int>(x, exponent);
   // int resultD = pow<double>(y, exponent);
   std::cout << x << "^" << exponent
             << " = " << resultI << std::endl;


   // Card* card = new Card(RED, 7);
   // Card* resultCard = pow<Card*>(card, exponent);
   // std::cout << card->getNumber() << "^" << exponent
   //           << " = " << resultCard->getNumber() << std::endl;

   return EXIT_SUCCESS;
}

// int pow(int base, int exponent) {
//    int result = 1;

//    for (int i = 0; i != exponent; ++i) {
//       result *= base;
//    }

//    return result;
// }

// float pow(float base, int exponent) {
//    float result = 1;

//    for (int i = 0; i != exponent; ++i) {
//       result *= base;
//    }

//    return result;
// }

// double pow(double base, int exponent) {
//    double result = 1;

//    for (int i = 0; i != exponent; ++i) {
//       result *= base;
//    }

//    return result;
// }
