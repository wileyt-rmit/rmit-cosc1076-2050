
#ifndef WEEK09_BST_H
#define WEEK09_BST_H

#include "BST_Node.h"

#include <memory>

class BST {
public:

   BST();
   ~BST();

   void clear();
   
   // contract: we don't modify the data given
   //           we don't modify the BST
   bool contains(const int data) const;

   // contract: we don't modify the data given
   void add(const int data);

   // Do a Depth First Search of the BST
   void dfs() const;

private:
   std::shared_ptr<BST_Node> root;

   bool contains(std::shared_ptr<BST_Node> node, 
                 const int data) const;

   std::shared_ptr<BST_Node> add(std::shared_ptr<BST_Node> node, 
                                 const int data);

   void dfs(std::shared_ptr<BST_Node> node) const;
};

#endif // WEEK09_BST_H
