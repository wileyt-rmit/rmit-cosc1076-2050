
#include "BST.h"

#include <iostream>
#include <string>

BST::BST() {
   root = nullptr;
}

BST::~BST() {
}

void BST::clear() {
   root = nullptr;
}

bool BST::contains(const int data) const {
   return contains(root, data);
}

bool BST::contains(std::shared_ptr<BST_Node> node, 
                   const int data) const {
   bool returnValue = false;

   if (node == nullptr) {
      returnValue = false;
   } else if (data == node->value) {
      returnValue = true;
   } else {
      // recursive case
      // check which side of the tree to search in
      if (data < node->value) {
         // go left
         returnValue = contains(node->left, data);
      } else {
         // go right
         returnValue = contains(node->right, data);
      }
   }

   return returnValue;
}

void BST::add(const int data) {
   std::cout << "*** Insert " << data << std::endl;
   root = add(root, data);
}

std::shared_ptr<BST_Node> BST::add(std::shared_ptr<BST_Node> node, 
                                   const int data) {

   // Please start here VSCODE
   std::shared_ptr<BST_Node> returnNode;

   if (node == nullptr) {
      // base case
      std::cout << "Make new new node: " << data << std::endl;
      returnNode = std::make_shared<BST_Node>(data);
   } else {
      // recursive case
      // check which side of the tree to add the data to
      if (data < node->value) {
         // go left
         std::cout << "\tInsert: " << data << " left of: " << node->value << std::endl;
         std::shared_ptr<BST_Node> subtree = add(node->left, data);
         node->left = subtree;
         returnNode = node;
      } else {
         // go right
         std::cout << "\tInsert: " << data << " right of: " << node->value << std::endl;
         std::shared_ptr<BST_Node> subtree = add(node->right, data);
         node->right = subtree;
         returnNode = node;
      }
   }

   return returnNode;
}


void BST::dfs() const {
   std::cout << "DFS:" << std::endl;
   dfs(root);
}

void BST::dfs(std::shared_ptr<BST_Node> node) const {
   // print out all node in the BST
   // go left
   if (node != nullptr) {
      dfs(node->left);

      // do me
      std::cout << " " << node->value << std::endl;

      // go right
      dfs(node->right);
   }
}
