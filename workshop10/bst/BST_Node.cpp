
#include "BST_Node.h"

#include <iostream>

using std::string;
using std::cout;
using std::endl;

BST_Node::BST_Node(int value) :
   value(value),
   left(nullptr),
   right(nullptr)
{
   cout << "BST_Node Create: " << value << endl;
}

BST_Node::BST_Node(const BST_Node& other) :
   value(other.value),
   left(other.left),
   right(other.right)
{
   cout << "BST_Node Copy: " << value << endl;
}

BST_Node::~BST_Node() {
   cout << "BST_Node Deconstructor: " << value << endl;
   left = nullptr;
   right = nullptr;
}
