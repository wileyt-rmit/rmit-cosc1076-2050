#include <iostream>
#include <memory>

#include "Boat.h"

// void printBoat(Boat* boat, int number);
void printBoat(std::shared_ptr<Boat> boat, int number);

int main(void) {

    // Boat* boat = new Boat();
    std::shared_ptr<Boat> boat = std::make_shared<Boat>();
    
    printBoat(boat, 1);
    boat->setName("Nautical");
    printBoat(boat, 1);

    // Duplicate boat 1
    {
        std::cout << "duplicate!" << std::endl;
        // Boat* boat2 = new Boat(*boat);
        std::shared_ptr<Boat> boat2 = std::make_shared<Boat>(*boat);
        printBoat(boat, 1);
        printBoat(boat2, 2);

        std::cout << "change some stuff" << std::endl;
        boat2->setName("Knot Again");
        boat2->getRudder()->setSteer(LEFT);
        boat2->getMast()->getSail()->setColour("white");
        boat->getMast()->setLength(5);
        printBoat(boat2, 2);
        // boat2 goes out of scope
        // therefore, memory for the smart pointer is deleted!!
    }

    printBoat(boat, 1);

    // cleaup
    // delete boat;
    // delete boat2;

    return EXIT_SUCCESS;
}

// void printBoat(Boat* boat, int number) {
void printBoat(std::shared_ptr<Boat> boat, int number) {
    std::cout << "Boat " << number
              << " Name: " << boat->getName()
              << ", Mast length: " << boat->getMast()->getLength()
              << ", Colour Sail: " << boat->getMast()->getSail()->getColour()
              << ", Rudder steering: " << boat->getRudder()->getSteer()
              << std::endl;
}
