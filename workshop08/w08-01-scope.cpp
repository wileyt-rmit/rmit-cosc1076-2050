#include <iostream>

#include <memory>

#define EXIT_SUCCESS    0

using std::cout;
using std::endl;

void foo(int x);

int main(void) {

   // Y Only enters scope
   int y = 7;
   cout << y << endl;

   // X Enters scope here
   int x = 1;
   cout << x << endl;
   {
      int x = 2;
      cout << x << endl;
      foo(3);
   }
   cout << x << endl;

   x = 9;

   foo(6);

   return EXIT_SUCCESS;

   // x goes out of scope here
   // y goes out of scope here
}

void foo(int x) {
   // parameter x exists here
   cout << x << endl;

   // x goes out of scope here
}
