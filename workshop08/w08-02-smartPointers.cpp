
#include <iostream>
#include <string>
#include <memory>

std::unique_ptr<int> getUnique();

int main(void) {
   // int* basicPtr = new int(5);
   // std::cout << basicPtr << std::endl;
   // std::cout << *basicPtr << std::endl;
   // delete basicPtr;

   std::unique_ptr<int> intPtr = std::make_unique<int>(10);
   std::cout << intPtr << std::endl;
   std::cout << *intPtr << std::endl;

   std::shared_ptr<int> sharedPtr = std::make_shared<int>(7);
   std::cout << sharedPtr << std::endl;
   std::cout << *sharedPtr << std::endl;

   std::shared_ptr<int> sharedPtr2 = sharedPtr;
   std::cout << sharedPtr2 << std::endl;
   std::cout << *sharedPtr2 << std::endl;

   *sharedPtr = -3;
   std::cout << *sharedPtr << std::endl;
   std::cout << *sharedPtr2 << std::endl;

   std::unique_ptr<int> functionPtr = getUnique();
   std::cout << *functionPtr << std::endl;

   return EXIT_SUCCESS;
}

std::unique_ptr<int> getUnique() {
   std::unique_ptr<int> intPtr = std::make_unique<int>(40);
   return intPtr;
}
