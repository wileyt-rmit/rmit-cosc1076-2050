
#include "Card.h"

class Node {
public:
   Card* card;
   Node* next;
   Node* prev;
};

class LinkedList {
public:
   LinkedList();
   LinkedList(LinkedList& other);
   ~LinkedList();

   int size();

   Card* get(int index);

   void add_front(Card* data);
   void add_back(Card* data);

   // Contract: There must be elements in the list to remove
   void remove_front();
   void remove_back();

   void clear();

private:
   Node* head;
   Node* tail;

   // int size;
};
