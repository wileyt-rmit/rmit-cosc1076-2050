#include "DE_LinkedList.h"

#include <stdexcept>

LinkedList::LinkedList() {
   head = nullptr;
   tail = nullptr;
}

LinkedList::LinkedList(LinkedList& other) {
}

LinkedList::~LinkedList() {
   clear();
}

int LinkedList::size() {
   int counter = 0;

   Node* current = head;
   while (current != nullptr) {
      current = current->next;
      ++counter;
   }

   return counter;
}

Card* LinkedList::get(int index) {
   Card* retCard = nullptr;

   if (index >= 0 && index < size()) {
      int counter = 0;
      Node* current = head;
      while (counter < index) {
         current = current->next;
         ++counter;
      }

      retCard = current->card;
   }

   return retCard;
}

void LinkedList::add_front(Card* data) {
   Node* node = new Node();
   node->card = data;
   node->next = head;
   node->prev = nullptr;

   // Update previous head!!
   if (head == nullptr) {
      tail = node;
   } else {
      head->prev = node;
   }
      
   // Update head
   head = node;
}

void LinkedList::add_back(Card* data) {

}

void LinkedList::remove_front() {
   if (head != nullptr) {
      Node* toDelete = head;
      // head = toDelete->next;
      head = head->next;

      // Update previous pointer
      if (head == nullptr) {
         tail = nullptr;
      } else {
         head->prev = nullptr;
      }

      delete toDelete->card;
      delete toDelete;
   }
}

void LinkedList::remove_back() {
   if (tail != nullptr) {
      Node* toDelete = tail;
      // head = toDelete->next;
      tail = tail->prev;

      // Update previous pointer
      if (tail == nullptr) {
         head = nullptr;
      } else {
         tail->next = nullptr;
      }

      delete toDelete->card;
      delete toDelete;
   }
}

void LinkedList::clear() {
   while (head != nullptr) {
      remove_front();
   }
}