#include "LinkedList.h"

#include <stdexcept>

Node::Node() {
   card = nullptr;
   next = nullptr;
}

Node::~Node() {
   // std::cout << "~Node()" << std::endl;
   card = nullptr;
   next = nullptr;
}

LinkedList::LinkedList() {
   head = nullptr;
}

LinkedList::LinkedList(LinkedList& other) {
}

LinkedList::~LinkedList() {
   clear();
}

int LinkedList::size() {
   int counter = 0;

   std::shared_ptr<Node> current = head;
   while (current != nullptr) {
      current = current->next;
      ++counter;
   }

   return counter;
}

std::shared_ptr<Card> LinkedList::get(int index) {

   std::shared_ptr<Card> retCard = nullptr;

   if (index >= 0 && index < size()) {
      int counter = 0;
      std::shared_ptr<Node> current = head;
      while (counter < index) {
         current = current->next;
         ++counter;
      }

      retCard = current->card;
   }

   return retCard;
}

void LinkedList::add_front(std::shared_ptr<Card> data) {
   // Node* node = new Node();
   std::shared_ptr<Node> node = std::make_shared<Node>();
   node->card = data;
   node->next = head;
   head = node;
}

void LinkedList::add_back(std::shared_ptr<Card> data) {

}

void LinkedList::remove_front() {
   if (head != nullptr) {
      // std::shared_ptr<Node> toDelete = head;
      // head = toDelete->next;
      head = head->next;

      // toDelete = nullptr;
   }
}

void LinkedList::remove_back() {
   // if (head != nullptr) {
      std::shared_ptr<Node> current = head;
      // Points to the node *before* the current
      std::shared_ptr<Node> prev = nullptr;

      // Find the node I want to delete
      while (current->next != nullptr) {
         prev = current;
         current = current->next;
      }

      if (prev != nullptr) {
         prev->next = nullptr;
      } else {
         // If previous is null, list has only one element
         // Therefore update head instead
         head = nullptr;
      }

      // delete current->card;
      // delete current;
   // } else {
   //    throw std::runtime_error("Nothing to remove");
   // }
}

void LinkedList::clear() {
   // while (head != nullptr) {
   //    remove_front();
   // }
   head = nullptr;
}