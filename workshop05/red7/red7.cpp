
/**
 * Topics:
 * 
 * 1. Revision
 *  - Objects
 *  - Dynamic Memoery Management
 *  - Standard Input I/O
 * 2. Program layout
 *  - Declarations / Definitions
 *  - Forward Delcarations
 *  - Overloading Functions/Methods/Constructors
 * 3. Program I/O
 *  - STL classes
 *  - File I/O
 *  - I/O abstraction
 * 4. String
 *  - Methods
 *  - String I/O
 * 5. Command Line Arguments
 * 6. Randomness
 */

#include <fstream>
#include <iostream>
#include <random>
#include <string>

#include "Card.h"
#include "Rule.h"
#include "Player.h"
#include "utils.h"

#define NUM_COLOURS     7
#define NUM_NUMS        7
#define MAX_SIZE_DECK (NUM_COLOURS * NUM_NUMS)

void readOneCard(std::ifstream& file, Colour* colour, int* number);

void setupGame(std::string deckFile, Card* deck[], Player* player1);
void playTheGame(Player* player1);
void cleanupGame(Player* player1);

int main(int argc, char** argv) {
  
   // Process our command-line args
   std::string deckFilename = "deck.txt";
   if (argc >= 2) {
      deckFilename = argv[1];
   }

   // Array of Card objects -> cards array is ON the STACK
   // Card OBJECTS will be on the HEAP
   Card* deck[MAX_SIZE_DECK];
   Player* player1 = new Player();
   for(int i =0; i < MAX_HAND_SIZE; ++i) {
      player1->hand[i] = nullptr;
   }
   player1->nCardsInHand = 0;

   // Setup the game (using the defined data structures)
   setupGame(deckFilename, deck, player1);

   // Play the game
   playTheGame(player1);

   // Cleanup - delete my array of objects
   cleanupGame(player1);

   return EXIT_SUCCESS;
}

void setupGame(std::string deckFile, Card* deck[], Player* player1) {
   // 1. open the file to read
   std::ifstream file(deckFile);

   // Read a set number of cards from the user or until EOF
   int numRead = 0;
   while(!file.eof() && numRead < MAX_SIZE_DECK) {
      Colour colour = RED;
      int number = 0;   
      readOneCard(file, &colour, &number);
      
      if (!file.eof()) {
         // Put the card in the array
         Card* card = new Card(colour, number);
         deck[numRead] = card;

         // Increment cards read
         ++numRead;
      }
   }

   // Close the file
   file.close();

   // Read in the deck -> create the players hand
   // for (int i = 0 ; i < MAX_HAND_SIZE; ++i) {
   //    // Tranfser OWNERSHIP!!!
   //    player1->hand[i] = deck[i];
   //    deck[i] = nullptr;
   //    ++player1->nCardsInHand;
   // }

   // Randomise the players hand!!!!
   std::random_device randomSeed;
   // std::default_random_engine fixedSeed(args->seed);
   std::uniform_int_distribution<int> uniform_dist(0, numRead-1);
   int i = 0;
   while (i < MAX_HAND_SIZE) {
      // Pick a random card from the deck!!!!
      int randIndex = uniform_dist(randomSeed);

      // Tranfser OWNERSHIP!!!
      if (deck[randIndex] != nullptr) {
         player1->hand[i] = deck[randIndex];
         deck[i] = nullptr;
         ++player1->nCardsInHand;
         ++i;
      }
   }

   // When we had array's before - print out all the cards
   // numRead is guaratenteed to be no longer than length of the array
   std::ofstream output("log.txt");
   printAllCards(output, deck, numRead);
   output.close();
}

void playTheGame(Player* player1) {
   std::cout << "Playing this Game " << std::endl;
   // Simple game
   Rule* rule = new Rule();

   // Keep setting cards until we run out
   int move = 0;
   while(!std::cin.eof() && player1->nCardsInHand > 0) {
      std::cout << std::endl
                << "Here are the cards in your hand: "
                << std::endl;
      printAllCards(player1->hand, MAX_HAND_SIZE);

      // Choose a card to move to the palette

      // Choose rule to play
      if (!std::cin.eof()) {
         std::cout << "Which card (choose index) do you want "
                   << "to move to the Rule?"
                   << std::endl;
         std::cin >> move;
         if (move >= 0 && move < MAX_HAND_SIZE) {
            if(player1->hand[move] != nullptr) {
               // Transfering ownership - Week 04
               rule->setRule(player1->hand[move]);
               player1->hand[move] = nullptr;
               --player1->nCardsInHand;
            }
         }
      }
   }

   // Cleanup!
   delete rule;
}

void cleanupGame(Player* player1) {
   for(int i =0; i < MAX_HAND_SIZE; ++i) {
      if (player1->hand[i] != nullptr) {
         delete player1->hand[i];
      }
   }
}

void readOneCard(std::ifstream& file, Colour* colour, int* number) {
   // Read Colour
   int readColour = 0;
   file >> readColour;
   if (readColour == 0) {
      *colour = RED;
   } else if (readColour == 1) {
      *colour = ORANGE;
   } else if (readColour == 2) {
      *colour = YELLOW;
   } else if (readColour == 3) {
      *colour = GREEN;
   } else if (readColour == 4) {
      *colour = BLUE;
   } else if (readColour == 5) {
      *colour = INDIGO;
   } else if (readColour == 6) {
      *colour = VIOLET;
   }

   // Read Number
   file >> *number;
}
