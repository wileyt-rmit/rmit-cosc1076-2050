#ifndef PLAYER_H
#define PLAYER_H 

#define MAX_HAND_SIZE       5
#define MAX_PALETTE_SIZE    MAX_HAND_SIZE

// Forward declare
class Card;

// Store information togther about the player
// Encapsulate information about the player
class Player {
public:
    // Do at home yourself!!!
    // Player();
    // ~Player();

    // Hand
    Card* hand[MAX_HAND_SIZE];
    int nCardsInHand;

    // Palatte

private:
};

#endif // PLAYER_H
