#ifndef B_H
#define B_H 

// #include "A.h"
class A;

class B {
public:
   B();
   B(B &other);
   ~B();
    
private:
   A* a;
};

#endif // B_H
