#include <iostream>

int main(int argc, char** argv) {

    std::cout << "Received Command Line Arguements" << std::endl;

    std::cout << "Number of args: " << argc << std::endl;
    for (int i = 0; i < argc; ++i) {
        std::string strArgv(argv[i]);
        std::cout << i << ": " << strArgv
                  << " has length: " << strArgv.size()
                  << std::endl;
    }

    return EXIT_SUCCESS;
}
