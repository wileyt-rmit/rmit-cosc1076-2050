
#include "Deck.h"

#include <iostream>
#include <fstream>

Deck::Deck() {
   deck = new LinkedList();
}

Deck::Deck(Deck& other)
{
   deck = new LinkedList(*other.deck);
}

Deck::~Deck() {
   delete deck;
}

int Deck::size() {
   return deck->size();
}

Card* Deck::get(int index) {
   return deck->get(index);
}

void Deck::add(Card* card) {
   deck->add_back(card);
}

void Deck::add(Card* card, int index) {
}

void Deck::remove(int index) {
}

void Deck::clear() {
   deck->clear();
}

