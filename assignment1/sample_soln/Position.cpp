
#include "Position.h"

Position::Position(int x, int y, Orientation orientation) :
   x(x), y(y), orientation(orientation)
{}

Position::Position(Position& other) :
   x(other.x), y(other.y),
   orientation(other.orientation)
{}

Position::~Position() {
}

int Position::getX() {
   return x;
}

int Position::getY() {
   return y;
}

Orientation Position::getOrientation() {
   return orientation;
}

bool Position::equals(Position& other) {
   return x == other.x
          && y == other.y
          && orientation == other.orientation
          ;
}

void Position::positionToRight(int& newX, int& newY) {
   newX = x;
   newY = y;

   if (orientation == ORIEN_NORTH) {
      ++newX;
   } else if (orientation == ORIEN_EAST) {
      ++newY;
   } else if (orientation == ORIEN_SOUTH) {
      --newX;
   } else if (orientation == ORIEN_WEST) {
      --newY;
   }
}

void Position::positionForward(int& newX, int& newY) {
   newX = x;
   newY = y;

   if (orientation == ORIEN_NORTH) {
      --newY;
   } else if (orientation == ORIEN_EAST) {
      ++newX;
   } else if (orientation == ORIEN_SOUTH) {
      ++newY;
   } else if (orientation == ORIEN_WEST) {
      --newX;
   }
}
