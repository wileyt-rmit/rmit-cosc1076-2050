
#include "WallFollower.h"

#include <iostream>
#include <stdexcept>

WallFollower::WallFollower()
{
   this->path = nullptr;
}

WallFollower::~WallFollower() {
   if (path != nullptr) {
      delete path;
   }
}

void WallFollower::execute(Maze maze) {
   // Delete old path if there is one
   if (path != nullptr) {
      delete path;
   }
   path = new Trail();

   // Find start robot position and goal position
   Position* robot = nullptr;
   Position* goal = nullptr;
   for (int y = 0; y != MAZE_DIM; ++y) {
      for (int x = 0; x != MAZE_DIM; ++x) {
         if (maze[y][x] == SYMBOL_GOAL) {
            goal = new Position(x, y, ORIEN_NORTH);
         } else if (maze[y][x] != SYMBOL_WALL && 
                     maze[y][x] != SYMBOL_EMPTY) {
            Orientation orien = ORIEN_NORTH;
            if (maze[y][x] == SYMBOL_NORTH) {
               orien = ORIEN_NORTH;
            } else if (maze[y][x] == SYMBOL_EAST) {
               orien = ORIEN_EAST;
            } else if (maze[y][x] == SYMBOL_SOUTH) {
               orien = ORIEN_SOUTH;
            } else if (maze[y][x] == SYMBOL_WEST) {
               orien = ORIEN_WEST;
            }
            robot = new Position(x, y, orien);
         }
      }
   }
   if(robot == nullptr) {
      throw std::runtime_error("Invalid Maze - no start point");
   } else if (goal == nullptr) {
      throw std::runtime_error("Invalid Maze - no end point");
   }

   // Loop
   bool done = false;

   path->addCopy(robot);
   while (!done) {
      // Get location to right-side
      int rightX = robot->getX();
      int rightY = robot->getY();
      robot->positionToRight(rightX, rightY);

      // Get location to the front of the robot
      int frontX = robot->getX();
      int frontY = robot->getY();
      robot->positionForward(frontX, frontY);

      // New robot position
      Position* newPos = nullptr;

      // Check if right is a wall
      if (maze[rightY][rightX] == SYMBOL_WALL) {
         if (maze[frontY][frontX] == SYMBOL_EMPTY) {
            // If front is empty, move forward
            newPos = new Position(frontX, frontY, robot->getOrientation());
            path->addCopy(newPos);
         } else {
            // Turn left
            Orientation newOrien = robot->getOrientation();
            if (robot->getOrientation() == ORIEN_NORTH) {
               newOrien = ORIEN_WEST;
            } else if (robot->getOrientation() == ORIEN_EAST) {
               newOrien = ORIEN_NORTH;
            } else if (robot->getOrientation() == ORIEN_SOUTH) {
               newOrien = ORIEN_EAST;
            } else if (robot->getOrientation() == ORIEN_WEST) {
               newOrien = ORIEN_SOUTH;
            }

            // Update position
            newPos = new Position(robot->getX(), robot->getY(), newOrien);
            path->addCopy(newPos);
         }
      } else {
         // If right is not a wall

         // Turn right
         Orientation newOrien = robot->getOrientation();
         if (robot->getOrientation() == ORIEN_NORTH) {
            newOrien = ORIEN_EAST;
         } else if (robot->getOrientation() == ORIEN_EAST) {
            newOrien = ORIEN_SOUTH;
         } else if (robot->getOrientation() == ORIEN_SOUTH) {
            newOrien = ORIEN_WEST;
         } else if (robot->getOrientation() == ORIEN_WEST) {
            newOrien = ORIEN_NORTH;
         }

         // Update position
         newPos = new Position(robot->getX(), robot->getY(), newOrien);
         path->addCopy(newPos);

         // Move forward
         frontX = newPos->getX();
         frontY = newPos->getY();
         newPos->positionForward(frontX, frontY);

         // Update position
         Position* newPos2 = new Position(frontX, frontY, newPos->getOrientation());
         path->addCopy(newPos2);

         // Reset newPos
         delete newPos;
         newPos = newPos2;
      }

      // Sanity 
      if (newPos == nullptr) {
         throw std::runtime_error("Failed to update new robot position");
      }

      // Update robot position
      delete robot;
      robot = newPos;
      
      // Check if at the goal
      if (robot->getX() == goal->getX() &&
          robot->getY() == goal->getY()) {
         done = true;
      }
   }

   // Cleanup
   delete robot;
   delete goal;
}

Trail* WallFollower::getFullPath() {
   return new Trail(*path);
}

