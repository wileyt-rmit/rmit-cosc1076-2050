
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>

#include "Types.h"
#include "WallFollower.h"


// Load a maze from standard input.
// Does not do much error checking.
void loadMazeStdin(Maze maze);

// Print out a Maze
void printMazeStdout(Maze maze, Trail* solution);

void printWalkingStdout(Trail* solution);

int main(int argc, char** argv) {
    // Load Maze from stdin
    Maze maze;
    loadMazeStdin(maze);

    // Solve using WallFollower
    WallFollower* follower = new WallFollower();
    Trail* fullPath = nullptr;
    follower->execute(maze);
    fullPath = follower->getFullPath();

    // Print Maze to stdout
    printMazeStdout(maze, fullPath);

    delete fullPath;

    return EXIT_SUCCESS;
}

void loadMazeStdin(Maze maze) {
    int charsRead = 0;
    char readC;
    for (int row = 0; row != MAZE_DIM; ++row) {
        for (int col = 0; col != MAZE_DIM; ++col) {
            if (std::cin.good()) {
                std::cin >> readC;
                maze[row][col] = readC;
                ++charsRead;
            }
        }

        // newline - recall whitespace is ignored with 'std::cin >>'
    }

    if (charsRead != (MAZE_DIM * MAZE_DIM)) {
        std::cerr << "ERROR: Only read " << charsRead << " maze cells - input file incorrectly formatted\n\n";
    }
}


void printMazeStdout(Maze maze, Trail* solution) {
    // Update maze with solution
    for (int i = 0; i != solution->size(); ++i) {
        Position* trail = solution->getPosition(i);
        char output = SYMBOL_EMPTY;
        if (trail->getOrientation() == ORIEN_NORTH) {
            output = SYMBOL_NORTH;
        } else if (trail->getOrientation() == ORIEN_EAST) {
            output = SYMBOL_EAST;
        } else if (trail->getOrientation() == ORIEN_SOUTH) {
            output = SYMBOL_SOUTH;
        } else if (trail->getOrientation() == ORIEN_WEST) {
            output = SYMBOL_WEST;
        }
        if (maze[trail->getY()][trail->getX()] != SYMBOL_GOAL) {
            maze[trail->getY()][trail->getX()] = output;
        }
    }

    for (int row = 0; row != MAZE_DIM; ++row) {
        for (int col = 0; col != MAZE_DIM; ++col) {
            std::cout << maze[row][col];
        }
        std::cout << std::endl;
    }
}
