
#include "Trail.h"

#include <iostream>

Trail::Trail() {
   for (int i = 0; i != TRAIL_ARRAY_MAX_SIZE; ++i) {
      positions[i] = nullptr;
   }
   length = 0;
}

Trail::Trail(Trail& other) :
   Trail()
{
   length = other.length;
   for (int i = 0; i != length; ++i) {
      positions[i] = new Position(*other.positions[i]);
   }
}

Trail::~Trail() {
   for (int i = 0; i != length; ++i) {
      delete positions[i];
      positions[i] = nullptr;
   }
}

int Trail::size() {
   return length;
}

Position* Trail::getPosition(int i) {
   Position* p = nullptr;
   if (i >= 0 && i < length) {
      p = positions[i];
   }

   return p;
}

void Trail::addCopy(Position* t) {
   Position* tCpy = new Position(*t);
   if (length != TRAIL_ARRAY_MAX_SIZE) {
      positions[length] = tCpy;
      ++length;
   }
}

bool contains(int x, int y) {
   return false;
}
