
#ifndef COSC_ASSIGN_ONE_Position
#define COSC_ASSIGN_ONE_Position

#include "Types.h"

class Position {
public:

   /*                                           */
   /* DO NOT MOFIFY ANY CODE IN THIS SECTION    */
   /*                                           */

   // Constructor/Desctructor
   Position(int x, int y, Orientation orientation);
   ~Position();

   // x-co-ordinate of the particle
   int getX();

   // y-co-ordinate of the particle
   int getY();

   // Orientation the robot is facing
   Orientation getOrientation();

   /*                                           */
   /* YOU MAY ADD YOUR MODIFICATIONS HERE       */
   /*                                           */
   Position(Position& other);
   bool equals(Position& other);

   // Get location to the 'right' of the robot
   void positionToRight(int& newX, int& newY);

   // Get position forward of the robot
   void positionForward(int& newX, int& newY);

private:

   /*                                           */
   /* DO NOT MOFIFY THESE VARIABLES             */
   /*                                           */
   int x;
   int y;
   Orientation orientation;


   /*                                           */
   /* YOU MAY ADD YOUR MODIFICATIONS HERE       */
   /*                                           */
};

#endif // COSC_ASSIGN_ONE_Position
